package cn.itcast.service.controller;

import cn.itcast.service.client.UserClient;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("user")
public class UserController {

    @Autowired
    private UserClient userClient;

    @RequestMapping("{id}")
    @HystrixCommand
    public String queryBid(@PathVariable("id") Long id) {
        return userClient.queryBid(id).toString();
    }



}
