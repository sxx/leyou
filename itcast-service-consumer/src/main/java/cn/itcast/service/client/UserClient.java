package cn.itcast.service.client;

import cn.itcast.service.pojo.User;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@FeignClient("service-provider")
public interface UserClient {


    @RequestMapping("user/{id}")
    public User queryBid(@PathVariable("id") Long id);
}
